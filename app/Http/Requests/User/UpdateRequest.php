<?php
namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules()
	{
		$id = $this->user;
    	return $rules = [
			'username' => 'required|max:255|unique:users,username,'.$id, 
			'email' => 'required|email|unique:users,email,'.$id
		];
	}
}
