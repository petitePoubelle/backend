<?php

namespace App\Http\Requests\Dumps;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'integer',
            'trash_id' => 'integer',
            'full' => 'boolean',
            'date' => 'date_format:"Y-m-d"'
        ];
    }
}
