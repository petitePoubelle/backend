<?php 
namespace App\Http\Controllers\V1\UserAccount;

use Illuminate\Routing\Controller;
use App\Http\Repositories\V1\UserAccount\SkillRepository;
use App\Http\Resources\V1\UserAccount\SkillResource;
use App\Http\Requests\Skills\CreateRequest;
use App\Http\Requests\Skills\UpdateRequest;
use Illuminate\Http\Request;
use Response;

class SkillController extends Controller {
	
	protected $skills;

    public function __construct(SkillRepository $skills)
    {
        $this->skills = $skills;
    }

    public function index()
    {
        return SkillResource::collection($this->skills->index());
    }

    public function show($id)
    {
        return  SkillResource::collection($this->skills->find($id));
    }

    public function store(CreateRequest $request)
    {
        $this->skills->store($request->all());
        return response()->json(['status'=>'success','msg'=>'Compétence créée']);
    }

    public function update(UpdateRequest $request, $id){
        $this->skills->update($request->all(), $id);
        return response()->json(['status'=>'success','msg'=>'Compétence modifiée']);
    }

    public function destroy($id){
        $this->skills->destroy($id);
        return response()->json(['status'=>'success','msg'=>'Compétence supprimée']);
    }

    public function sync(Request $request){
        $this->skills->sync($request->all());
        return response()->json(['status'=>'success','msg'=>'Compétence(s) associée(s)']);
    }
}