<?php 
namespace App\Http\Controllers\V1\Trashes;

use Illuminate\Routing\Controller;
use App\Http\Repositories\V1\Trashes\ContainerRepository;
use App\Http\Resources\V1\Trashes\ContainerResource;
use Response;

class ContainerController extends Controller {
	
	protected $containers;

    public function __construct(ContainerRepository $containers)
    {
        $this->containers = $containers;
    }

    public function index()
    {
        return ContainerResource::collection($this->containers->index());
    }

    public function show($id)
    {
        return ContainerResource::collection($this->containers->find($id));
    }
}