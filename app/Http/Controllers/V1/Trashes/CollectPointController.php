<?php 
namespace App\Http\Controllers\V1\Trashes;

use Illuminate\Routing\Controller;
use App\Http\Repositories\V1\Trashes\CollectPointRepository;
use App\Http\Resources\V1\Trashes\CollectPointResource;
use Response;

class CollectPointController extends Controller {
	
	protected $collect_points;

    public function __construct(CollectPointRepository $collect_points)
    {
        $this->collect_points = $collect_points;
    }

    public function index()
    {
        return CollectPointResource::collection($this->collect_points->index());
    }

    public function show($id)
    {
        return CollectPointResource::collection($this->collect_points->find($id));
    }
}