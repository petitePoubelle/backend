<?php 
namespace App\Http\Controllers\V1\Trashes;

use Illuminate\Routing\Controller;
use App\Http\Repositories\V1\Trashes\WasteRepository;
use App\Http\Resources\V1\Trashes\WasteResource;
use Response;

class WasteController extends Controller {
	
	protected $wastes;

    public function __construct(WasteRepository $wastes)
    {
        $this->wastes = $wastes;
    }

    public function index()
    {
        return WasteResource::collection($this->wastes->index());
    }

    public function show($id)
    {
        return WasteResource::collection($this->wastes->find($id));
    }
}