<?php
namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Repositories\V1\Auth\UserRepository;
use App\Http\Requests\User\RegisterRequest;
use Response;

class RegisterController extends Controller
{
    protected $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function register(RegisterRequest $request)
    {
        $this->users->store($request->all());
        return response()->json(['status'=>'success','msg'=>'utilisateur créé']);
    }
}
