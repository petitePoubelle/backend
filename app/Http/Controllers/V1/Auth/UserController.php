<?php 
namespace App\Http\Controllers\V1\Auth;

use Illuminate\Routing\Controller;
use App\Http\Repositories\V1\Auth\UserRepository;
use App\Http\Resources\V1\Auth\UserResource;
use App\Http\Requests\User\UpdateRequest;
use Response;

class UserController extends Controller {
	
	protected $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function index()
    {
        return UserResource::collection($this->users->index());
    }

    public function update(UpdateRequest $request, $id){
        $this->users->update($request->all(), $id);
        return response()->json(['status'=>'success','msg'=>'Utilisateur modifié']);
    }
}