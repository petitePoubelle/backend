<?php

namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\V1\Auth\UserResource;

class AuthController extends Controller
{
    /**
     * Get a JWT token via given credentials.
     *
     */
    
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'message' => 'Vous n\'êtes pas autorisé à vous identifier à cette application'
            ], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User
     *
     */
    public function me()
    {
        return new UserResource($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json([
            'status' => 'success',
            'message' =>'Successfully logged out'
        ]);
    }

    /**
     * Refresh a token.
     *
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }
    /**
     * Get the guard to be used during authentication.
     *
     */
    public function guard()
    {
        return Auth::guard();
    }
}
