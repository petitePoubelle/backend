<?php 
namespace App\Http\Repositories;

abstract class BaseRepository {
	/**
	 * The Model instance.
	 *
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;
	/**
	 * Get number of records.
	 *
	 * @return array
	 */
}