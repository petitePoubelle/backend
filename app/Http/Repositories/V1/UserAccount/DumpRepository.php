<?php namespace App\Http\Repositories\V1\UserAccount;

use App\Http\Repositories\BaseRepository;
use App\Models\Dump;

class DumpRepository extends BaseRepository
{
	public function __construct(Dump $dump)
	{
		$this->model = $dump;
	}
	public function index()
	{
		return $this->model->get();
	}
    
    public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $dump = new $this->model;
	    $dump->fill($inputs);
	    $dump->save();
	}

	public function update($inputs, $id)
	{		
		$dump = $this->model->find($id);
		$dump->fill($inputs);
		$dump->save();
	}
	public function destroy($id)
	{
		$dump = $this->model->find($id);
		$dump->delete();
    }  
}