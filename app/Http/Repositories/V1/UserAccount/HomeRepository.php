<?php namespace App\Http\Repositories\V1\UserAccount;

use App\Http\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\Home;
use App\Models\User;

class HomeRepository extends BaseRepository
{
	public function __construct(Home $home)
	{
		$this->model = $home;
	}
	public function index()
	{
		return $this->model->get();
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $home = new $this->model;
	    $home->fill($inputs);
		$home->save();
		$user = User::find(Auth::id());
		$user->home_id = $home->id;
		$user->save();
	}

	public function update($inputs, $id)
	{		
		$home = $this->model->find($id);
		$home->fill($inputs);
		$home->save();
	}
	
	public function destroy($id)
	{
		$home = $this->model->find($id);
		$home->delete();
	}  
}