<?php namespace App\Http\Repositories\V1\UserAccount;

use App\Http\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\Skill;
use App\Models\User;

class SkillRepository extends BaseRepository
{
	public function __construct(Skill $skill)
	{
		$this->model = $skill;
	}
	public function index()
	{
		return $this->model->get();
	}
    
    public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $skill = new $this->model;
	    $skill->fill($inputs);
	    $skill->save();
	}

	public function update($inputs, $id)
	{		
		$skill = $this->model->find($id);
		$skill->fill($inputs);
		$skill->save();
	}
	public function destroy($id)
	{
		$skill = $this->model->find($id);
		$skill->delete();
	}

	public function sync($inputs)
	{
		$user = User::find(Auth::id());
		$cats = array('vrac','bio','local','diy','progressGrey','progressTri');
		$skills = [];
		foreach($cats as $key=>$cat){
			if (!empty($inputs[$cat])) {
				array_push($skills,$inputs[$cat]);
			}
		}
		if ($inputs['compost'] == true) {
			array_push($skills,9);
		}
		$user->skills()->sync($skills);
		$points =$user->skills()->sum('points');
		
		$score =$user->score;
		$score->points = $points;
		$user->score()->save($score);
	}
}