<?php namespace App\Http\Repositories\V1\Advices;

use App\Http\Repositories\BaseRepository;
use App\Models\Advice;

class AdviceRepository extends BaseRepository
{
	public function __construct(Advice $advice)
	{
		$this->model = $advice;
	}
	public function index()
	{
		return $this->model->get();
	}
    
    public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $advice = new $this->model;
	    $advice->fill($inputs);
	    $advice->save();
	}

	public function update($inputs, $id)
	{		
		$advice = $this->model->find($id);
		$advice->fill($inputs);
		$advice->save();
	}
	public function destroy($id)
	{
		$advice = $this->model->find($id);
		$advice->delete();
    }  
}