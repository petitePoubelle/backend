<?php namespace App\Http\Repositories\V1\Trashes;

use App\Http\Repositories\BaseRepository;
use App\Models\CollectPoint;

class CollectPointRepository extends BaseRepository
{
	public function __construct(CollectPoint $collect_point)
	{
		$this->model = $collect_point;
	}
	public function index()
	{
		return $this->model->get();
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $collect_point = new $this->model;
	    $collect_point->fill($inputs);
	    $collect_point->save();
	}

	public function update($inputs, $id)
	{		
		$collect_point = $this->model->find($id);
		$collect_point->fill($inputs);
		$collect_point->save();
	}
	public function destroy($id)
	{
		$collect_point = $this->model->find($id);
		$collect_point->delete();
	}  
}