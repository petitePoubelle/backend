<?php namespace App\Http\Repositories\V1\Trashes;

use App\Http\Repositories\BaseRepository;
use App\Models\Container;

class ContainerRepository extends BaseRepository
{
	public function __construct(Container $container)
	{
		$this->model = $container;
	}
	public function index()
	{
		return $this->model->get();
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
	    $container = new $this->model;
	    $container->fill($inputs);
	    $container->save();
	}

	public function update($inputs, $id)
	{		
		$container = $this->model->find($id);
		$container->fill($inputs);
		$container->save();
	}
	public function destroy($id)
	{
		$container = $this->model->find($id);
		$container->delete();
	}  
}