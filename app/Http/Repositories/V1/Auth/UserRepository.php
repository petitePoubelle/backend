<?php namespace App\Http\Repositories\V1\Auth;

use App\Http\Repositories\BaseRepository;
use App\Models\User;
use App\Models\Score;

class UserRepository extends BaseRepository
{
	public function __construct(User $user)
	{
		$this->model = $user;
	}

	public function index()
	{

		return $this->model->get();
	}

	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	public function store($inputs)
	{
		$user = new $this->model;
		$user->username = $inputs['username'];
        $user->email = $inputs['email'];
		if(isset($inputs['password'])){
	    	$user->password = bcrypt($inputs['password']);
	    }
	    if(isset($inputs['admin'])){
	    	$user->admin = $inputs['admin'];
		}
		$user->save();
		$score = new Score;
		$score->points = 0;
		$user->score()->save($score);
	}

	public function update($inputs, $id)
	{		
		$user = $this->model->find($id);
		$user->username = $inputs['username'];
        $user->email = $inputs['email'];
		if(isset($inputs['password'])){
	    	$user->password = bcrypt($inputs['password']);
		}
		if(isset($inputs['admin'])){
	    	$user->admin = $inputs['admin'];
	    }
		$user->save();
	}
	public function destroy($id)
	{
		$user = $this->model->find($id);
		$user->delete();
	}
}