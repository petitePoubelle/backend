<?php

namespace App\Http\Resources\V1\UserAccount;

use Illuminate\Http\Resources\Json\JsonResource;

class LevelResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'level_name' => $this->level_name,
            'points' => $this->points
        ];;
    }
}