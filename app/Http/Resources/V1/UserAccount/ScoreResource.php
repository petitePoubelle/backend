<?php

namespace App\Http\Resources\V1\UserAccount;

use Illuminate\Http\Resources\Json\JsonResource;

class ScoreResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'points' => $this->points
        ];
    }
}