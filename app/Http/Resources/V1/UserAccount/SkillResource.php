<?php

namespace App\Http\Resources\V1\UserAccount;

use Illuminate\Http\Resources\Json\JsonResource;

class SkillResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->category,
            'description' => $this->description,
            'points' => $this->points
        ];
    }
}