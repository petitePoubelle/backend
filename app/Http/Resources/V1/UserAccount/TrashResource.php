<?php

namespace App\Http\Resources\V1\UserAccount;

use Illuminate\Http\Resources\Json\JsonResource;

class TrashResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'capacity' => $this->capacity,
        ];
    }
}