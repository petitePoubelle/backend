<?php

namespace App\Http\Resources\V1\UserAccount;

use Illuminate\Http\Resources\Json\JsonResource;

class DumpResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'trash_id' => (new TrashResource($this->trash))->id,
            'full' => $this->full,
            'date' => $this->date
        ];
    }
}