<?php

namespace App\Http\Resources\V1\Auth;

use App\Models\Level;
use App\Models\Trash;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\V1\UserAccount\HomeResource;
use App\Http\Resources\V1\UserAccount\TrashResource;
use App\Http\Resources\V1\UserAccount\ScoreResource;
use App\Http\Resources\V1\UserAccount\DumpResource;
use App\Http\Resources\V1\UserAccount\SkillResource;
use App\Http\Resources\V1\UserAccount\LevelResource;

class UserResource extends JsonResource
{

    public function points() {
        $points = 0;
        if (!empty((new ScoreResource($this->score))->points)){
            $points = (new ScoreResource($this->score))->points;
        }
        return $points;
    }

    public function level() {
        $level = Level::where('points', '<=', $this->points())->get();
        return $level;
    }

    public function home() {
        $home = '';
        if (!empty(new HomeResource($this->home))){
            $home = new HomeResource($this->home);
        }
        return $home;        
    }

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'email' => $this->email,
            'registration' => $this->created_at,
            'admin' => $this->admin,
            'points' => $this->points(),
            'level' => $this->level()->last()->level_name,
            'home' => $this->home(),
            'trashes' => TrashResource::collection($this->trashes),
            'dumps' => DumpResource::collection($this->dumps),
            'skills' => SkillResource::collection($this->skills),
        ];
    }
}
