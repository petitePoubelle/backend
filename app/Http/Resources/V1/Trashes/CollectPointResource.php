<?php

namespace App\Http\Resources\V1\Trashes;

use Illuminate\Http\Resources\Json\JsonResource;

class CollectPointResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address' => $this->address,
            'city' => $this->city,
            'volume' => $this->volume,
            'waste_type' => $this->waste_type,
            'container_type' => $this->container_type,
            'lon_x' => $this->lon_x,
            'lat_y' => $this->lat_y
        ];
    }
}