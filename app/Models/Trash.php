<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trash extends Model
{
    protected $fillable = ['type', 'capacity'];

    public function users(){
        return $this->belongsToMany('App\Models\User');
    }
}
