<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $fillable = ['type', 'residents', 'gender'];

    public function users(){
        return $this->hasMany('App\Models\User');
    }
}