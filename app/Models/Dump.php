<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dump extends Model
{

    protected $fillable = ['user_id','trash_id','full', 'date'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function trash(){
        return $this->belongsTo('App\Models\Trash');
    }
}
