<?php
namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $table = 'users';

    protected $fillable = ['username', 'email', 'password'];
    protected $hidden = ['password'];

    public function home(){
        return $this->belongsTo('App\Models\Home');
    }

    public function trashes(){
        return $this->belongsToMany('App\Models\Trash');
    }

    public function skills(){
        return $this->belongsToMany('App\Models\Skill');
    }

    public function dumps(){
        return $this->hasMany('App\Models\Dump');
    }

    public function score(){
        return $this->hasOne('App\Models\Score');
    }

    

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     */
    public function getJWTCustomClaims()
    {
        return ['admin' => $this->admin];
    }

    public function isAdmin()
    {
        return $this->admin;
    }

}
