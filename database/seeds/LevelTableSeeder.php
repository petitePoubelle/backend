<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Level;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('levels')->delete();
        Level::create([
            'level_name' => 'Novice du déchet',
            'points' => 0
        ]);
        Level::create([
            'level_name' => 'Débutant.e',
            'points' => 25
        ]);
        Level::create([
            'level_name' => 'Intermédiaire',
            'points' => 50
        ]);
        Level::create([
            'level_name' => 'Confirmé.e',
            'points' => 75
        ]);
        Level::create([
            'level_name' => 'Pro',
            'points' => 110
        ]);
        Level::create([
            'level_name' => 'Expert.e de ma p\'tite poubelle',
            'points' => 150
        ]);
        Level::create([
            'level_name' => 'Ambassadrice.eur du zéro déchet',
            'points' => 200
        ]);
        Level::create([
            'level_name' => 'Boss de ma p\'tite poubelle',
            'points' => 300
        ]);
        Model::reguard();
    }
}