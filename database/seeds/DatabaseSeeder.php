<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(ContainersTableSeeder::class);
         $this->call(WastesTableSeeder::class);
         $this->call(SkillsTableSeeder::class);
         $this->call(ContainerWasteTableSeeder::class);
         $this->call(LevelTableSeeder::class);
         $this->call(TrashTableSeeder::class);
         $this->call(CollectPointsTableSeeder::class);
         //$this->call(UserTableSeeder::class);
         //$this->call(ScoreTableSeeder::class);
         //$this->call(HomeTableSeeder::class);
         //$this->call(TrashUserTableSeeder::class);
         //$this->call(UserSkillsTableSeeder::class);
         //$this->call(DumpTableSeeder::class);
         

    }
}
