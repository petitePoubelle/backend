<?php

use Illuminate\Database\Seeder;
use \App\Models\Dump;

class DumpTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Dump::class,2)->create();
    }
}
