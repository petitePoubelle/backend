<?php

use Illuminate\Database\Seeder;
use \App\Models\Home;

class HomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Home::class,2)->create();
    }
}
