<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TrashTableSeeder extends Seeder
{

    public function run()
    {
        Model::unguard();
        DB::table('trashes')->delete();
		DB::statement("
            insert into ptitepoubelle.trashes(id,type,capacity,created_at,updated_at) 
            select id,type,capacity,now(),now()
            from sources.trashes
            order by id;"
        );
        Model::reguard();
    }
}
