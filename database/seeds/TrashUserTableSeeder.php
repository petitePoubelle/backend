<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrashUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userIds = DB::table('users')->pluck('id')->all();
        
        $trashIds = DB::table('trashes')->pluck('id')->all();

        foreach((range(1,5)) as $index){
            DB::table('trash_user')->insert([
                'user_id' => $userIds[array_rand($userIds)],
                'trash_id' => $trashIds[array_rand($trashIds)],
            ]);
        }
    }
}
