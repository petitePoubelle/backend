<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userIds = DB::table('users')->pluck('id')->all();
        
        $skillIds = DB::table('skills')->pluck('id')->all();

        foreach((range(1,5)) as $index){
            DB::table('skill_user')->insert([
                'user_id' => $userIds[array_rand($userIds)],
                'skill_id' => $skillIds[array_rand($skillIds)],
            ]);
        }
    }
}
