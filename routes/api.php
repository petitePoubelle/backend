<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->namespace('V1')->group(function () {
    Route::prefix('auth')->namespace('Auth')->group(function () {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('register', 'RegisterController@register');
        Route::post('refresh', 'AuthController@refresh');
    });
    Route::namespace('Auth')->middleware('jwt.auth')->group(function () {
        Route::apiResource('users','UserController');
        Route::post('me', 'AuthController@me');
    });
    Route::namespace('UserAccount')->middleware('jwt.auth')->group(function () {
        Route::apiResource('dumps','DumpController');
        Route::apiResource('homes', 'HomeController');
        Route::post('trashes/sync', 'TrashController@sync');
        Route::apiResource('trashes', 'TrashController');
        Route::post('skills/sync', 'SkillController@sync');
        Route::apiResource('skills','SkillController');
    });
    Route::namespace('Trashes')->group(function () {
        Route::post('collect_points', 'CollectPointController@index');
        Route::post('wastes', 'WasteController@index');
        Route::post('containers', 'ContainerController@index');
    });

});


